# Dschungelbuch Siegen

Informationssammlung zu Orten in Siegen

    **Bearbeiten:**

Um Das Dschungelbuch zu bearbeiten könnt ihr diesen Ordner herrunterladen, in dem LaTeX editor der Wahl die dschungelbuch.tex Bearbeiten und schauen wie ihr das gerne hättet.

im Eintraege ordner findet ihr die einzelnen Einträge, die werden ganz unten in der dschungelbuch.tex datei Eingebunden, also könnt ihr beliebige neue Einträge in diesem Ordner erstellen, jeder Eintrag in einer eigenen Datei, die Reihenfolge definiert dann die dschungelbuch.tex

Möchtet ihr Änderungen vorschlagen könnt ihr einfach nachfragen ob eure Dateien von uns eingepflegt werden können.

    **Größere Änderungen (Änderungen in Kompliziert):**

Wenn es komplizierter wird könnt ihr idealerweise mit eurem eigenen Account eine Abspaltung (Fork) im GitLab Interface erstellen, der Button dafür ist derzeit oben rechts.

Diesen könnt ihr dann mit einem Git Programm runterladen, eine Abzweigung (Branch) erstellen und wenn ihr änderungen in diese Abzweigung eingefügt habt (Commit und Push) dann schlägt euch GitLab auf dieser Seite vor einen Anfrage (Pull Request) zu erstellen.

Eine solche Anfrage kann dann von allen diskutiert werden und euer Username taucht öffentlich im Internet zum Lobpreisen und Danken auf.